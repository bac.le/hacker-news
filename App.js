/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { View, } from 'react-native';
import {
  createAppContainer
} from 'react-navigation';

import { SafeAreaView } from 'react-navigation';

import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import Home from './views/HomeScreen';
import JobsScreen from './views/JobsScreen';
import NewScreen from './views/NewScreen';
import ShowScreen from './views/ShowScreen';
import DetailScreen from './views/DetailsScreen';
import AskScreen from './views/AskScreen';
import ViewCommentScreen from './views/ViewCommentScreen';


const TabNavigation = createMaterialTopTabNavigator({
  "Hacker News": {
    screen: Home,
  },
  New: {
    screen: NewScreen
  },
  Ask: {
    screen: AskScreen
  },
  Show: {
    screen: ShowScreen
  },
  Jobs: {
    screen: JobsScreen
  },
}, {
  defaultNavigationOptions: ({ navigation }) => {
    return {
      tabBarOptions: {
        style: {
          backgroundColor: '#043353',
          elevation: 0, // remove shadow on Android
          shadowOpacity: 0, // remove shadow on iOS,
        }, indicatorStyle: {
          height: '100%',
          backgroundColor: '#ff6900'
        }
      },
    };
  }
})

const stackNavigation = createStackNavigator({
  Root: TabNavigation,
  DetailScreen: {
    screen: DetailScreen,
  },
  ViewCommentScreen: {
    screen: ViewCommentScreen,
  },
}, {
  initialRouteName: 'Root',
  headerMode: 'none'
})

function InjectMain(Com) {
  class InjMan extends Component {
    render() {
      return (
        // <View>
        <SafeAreaView style={{ flexGrow: 1 }}>
          <Com {...this.props} />
        </SafeAreaView>
        // {/* <View style={{height: 16, backgroundColor:'black'}}/> */}
        // </View>

      )
    }
  }
  return InjMan;
}


export default InjectMain(createAppContainer(stackNavigation));