import constants from "jest-haste-map/build/constants";

const fetchData = (dataType) => {
    const result = { newsList: [], loading: false }
    return fetch(`https://hacker-news.firebaseio.com/v0/${dataType}.json?print=pretty`)
        .then(res => res.json())
        .then(res => ({ ...result, newsList: res.splice(0, 100), loading: true }))
        .catch(error => ({ ...result, error }))
}

export const fetchStories = async (dataType) => {
    var { newsList } = await fetchData(dataType);
    try {
        return Promise.all(newsList.map(item => {
            return fetch(`https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`)
                .then(res => res.json()).then(res => res);
        }));
    } catch (err) {
        console.log("Error fetching data-----------", err);
    }
}

export const crawlComment = async (commentIDList) => {
    return Promise.all(commentIDList.map(item => {
        return fetch(`https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`)
            .then(res => res.json()).then(res => res);
    }));
}

export const crawlCommentRecursive = async (commentIDList, resultList = []) => {
    const result = (await Promise.all(commentIDList.map(item =>
        fetch(`https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`)
            .then(res => res.json())
            .then(res => {
                if (res.kids instanceof Array) {
                  return  crawlComment(res.kids, resultList)
                }
            })
    ))).filter(item => item.length);
    // resultList.push(result)
   return resultList.push(result)
}
