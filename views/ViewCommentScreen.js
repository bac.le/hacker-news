import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { get as lodashGet } from 'lodash'
import { toRelativeTime } from '../utils/Utils'
import { URLRegex } from '../assets/commons'
import { crawlComment } from '../services/FetchService';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import HTMLView from 'react-native-htmlview';

function CommentItemView({ author, time, comment }) {
    return (
        <View style={{ marginStart: 16 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <Icon name='chess-knight' style={{ color: '#F95A37', marginTop: 4, marginEnd: 4 }} />
                <Text style={{ color: '#F95A37' }}>{author}
                    <Text> {toRelativeTime(time)}</Text>
                </Text>
            </View>
            <HTMLView style={{ marginStart: 16 }} value={comment}>{comment}</HTMLView>
        </View>
    );
}

class ViewCommentScreen extends React.Component {
    state = { item: lodashGet(this.props, 'navigation.state.params.item', ''), data: [] };

    componentDidMount() {
        this.fetchComment()
    }

    fetchComment = async () => {
        this.setState({ data: await crawlComment(this.state.item.kids, this.state.data) })
    }

    render() {
        let item = this.state.item
        return (
            <FlatList
                style={{ backgroundColor: '#F4F3EB' }}
                ListHeaderComponent={<View style={{ flex: 1, flexDirection: 'row' }}>
                    <Icon name='chess-knight' style={{ color: '#F95A37', margin: 4 }} />
                    <View>
                        <Text style={styles.title}>{item.title}
                            <Text style={styles.author}>{" (" + URLRegex.exec(item.url) + ")"}</Text>
                        </Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.info}>{item.score + " points"}</Text>
                            <Text style={styles.info}> by </Text>
                            <Text style={styles.author}>{item.by}</Text>
                            <Text style={styles.info}>{" " + toRelativeTime(item.time)}</Text>
                            <Text style={{ color: '#F95A37' }}> | </Text>
                            <Text style={styles.author}>{" " + item.descendants + " comments"}</Text>
                        </View>
                        <HTMLView style={{ marginStart: 4 }} value={item.text ? item.text : ""} />
                    </View>
                </View>}

                data={this.state.data.filter(item => item.by)}
                renderItem={({ item }) =>
                    <CommentItemView
                        author={item.by}
                        time={(item.time)}
                        comment={item.text}
                    />}
                keyExtractor={item => item.by + item.time}
                ItemSeparatorComponent={this.renderSeparator}
            />
        );
    }

    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 8,
                }}
            />
        );
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 8,
    },
    title: {
        fontSize: 16,
        color: '#F95A37',
    },
    item: {
        backgroundColor: '#F4F3EB',
        padding: 2,
        marginVertical: 1,
        marginStart: 8,
        marginEnd: 2,
    },
    index: {
        fontSize: 16,
        color: '#F95A37',
        marginEnd: 4,
    },
    resource: {
        fontSize: 12,
        marginStart: 4,
        marginEnd: 1,
        color: '#F95A37',
    },
    info: {
        fontSize: 12,
        color: '#F95A37',
    },
    author: {
        fontSize: 12,
        color: '#F95A37',
    },
});

export default ViewCommentScreen