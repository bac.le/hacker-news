import React from 'react';
import { View, FlatList, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { fetchStories } from '../services/FetchService';
import { DataType } from '../assets/commons'
import { toRelativeTime } from '../utils/Utils'

function Item({ title, index, time, url }) {
  return (
    <View style={styles.item}>
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <Text style={styles.index}>{index}</Text>
        <View style={{ marginBottom: 2 }}>
          <Text style={styles.title}>{title}
            <Text style={styles.author}>{url}</Text>
          </Text>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text style={styles.info}>{time}</Text>
          </View>
        </View>
      </View>

    </View>
  );
}

class JobsScreen extends React.Component {
  state = {
    data: [],
    isRefresing: false,
  };
  componentDidMount() {
    this.fetchNews()
  }

  fetchNews = async () => {
    this.setState({ isRefresing: true })
    const data = await fetchStories(DataType.Job);
    this.setState({ data })
    this.setState({ isRefresing: false })
  }

  _onPress(item) {
    this.props.navigation.navigate('DetailScreen', {
      URL: item.url,
    });

  }

  render() {
    return (
      <FlatList
        refreshing={this.state.isRefresing}
        data={this.state.data.filter(item => item.title)}
        renderItem={({ item, index }) =>
          <TouchableOpacity onPress={() => this._onPress(item)}>
            <Item
              title={item.title}
              index={index + 1 + '.'}
              time={toRelativeTime(item.time)}
            />
          </TouchableOpacity>}
        keyExtractor={item => item.title + item.by}

        onRefresh={() => {
          this.fetchNews();
        }
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
  },
  title: {
    fontSize: 16,
    color: '#F95A37',
  },
  item: {
    backgroundColor: '#F4F3EB',
    padding: 2,
    marginVertical: 1,
    marginStart: 8,
    marginEnd: 2,
  },
  index: {
    fontSize: 16,
    color: '#F95A37',
    marginEnd: 4,
  },
  resource: {
    fontSize: 12,
    marginStart: 4,
    marginEnd: 1,
    color: '#F95A37',
  },
  info: {
    fontSize: 12,
    color: '#F95A37',
  },
  author: {
    fontSize: 12,
    color: '#F95A37',
    textDecorationLine: 'underline',
  },
});

export default JobsScreen