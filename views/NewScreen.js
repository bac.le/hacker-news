import React from 'react';
import { View, FlatList, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { fetchStories } from '../services/FetchService';
import { URLRegex } from '../assets/commons'
import { DataType } from '../assets/commons'
import { toRelativeTime } from '../utils/Utils'

function Item({ title, index, point, author, time, comment, url, elementClick, }) {
  return (
    <View style={styles.item}>
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <Text style={styles.index}>{index}</Text>
        <View style={{ marginBottom: 2 }}>
          <Text style={styles.title}>{title}
            <Text style={styles.author}>{url}</Text>
          </Text>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Text style={styles.info}>{point}</Text>
            <Text style={styles.info}> by </Text>
            <Text style={styles.author}>{author}</Text>
            <Text style={styles.info}>{time}</Text>
            <Text style={{ color: '#F95A37' }}> | </Text>
            <TouchableOpacity onPress={() => elementClick()}>
              <Text style={styles.author}>{comment}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

    </View>
  );
}


class NewScreen extends React.Component {

  state = {
    data: [],
    isRefresing: false,
  };
  componentDidMount() {
    this.fetchNews()
  }

  fetchNews = async () => {
    this.setState({ isRefresing: true })
    const data = await fetchStories(DataType.News);
    this.setState({ data })
    this.setState({ isRefresing: false })
  }

  _onPress(item) {
    console.log({item})
    if (!item.url) {
      this.props.navigation.navigate('ViewCommentScreen', {
        item: item,
      })
    }

    if (item.url && !item.text) {
      this.props.navigation.navigate('DetailScreen', {
        URL: item.url,
      });
    }

  }
  _elementPress(item) {
    this.props.navigation.navigate('ViewCommentScreen', {
      item: item,
    })
  }

  render() {
    return (
      <FlatList
        data={this.state.data}
        refreshing={this.state.isRefresing}
        renderItem={({ item, index }) =>
          <TouchableOpacity onPress={() => this._onPress(item)}>
            <Item
              url={item.url ? ' (' + URLRegex.exec(item.url) + ')' : ""}
              title={item.title}
              index={index + 1 + '.'}
              point={item.score + " points"}
              time={toRelativeTime(item.time)}
              comment={item.descendants == 0 ? "discus" : item.descendants + " comments"}
              author={item.by + " "}
              elementClick={() => this._elementPress(item)}
            />
          </TouchableOpacity>}
        keyExtractor={item => item.title + item.by}
        onRefresh={() => {
          this.fetchNews();
        }}
      />
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
  },
  title: {
    fontSize: 16,
    color: '#F95A37',
  },
  item: {
    backgroundColor: '#F4F3EB',
    padding: 2,
    marginVertical: 1,
    marginStart: 8,
    marginEnd: 2,
  },
  index: {
    fontSize: 16,
    color: '#F95A37',
    marginEnd: 4,
  },
  resource: {
    fontSize: 12,
    marginStart: 4,
    marginEnd: 1,
    color: '#F95A37',
  },
  info: {
    fontSize: 12,
    color: '#F95A37',
  },
  author: {
    fontSize: 12,
    color: '#F95A37',
    textDecorationLine: 'underline',
  },
});

export default NewScreen