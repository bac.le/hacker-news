import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import { get as lodashGet } from 'lodash'

class DetailsScreen extends Component {

  state = { URL: lodashGet(this.props, 'navigation.state.params.URL', '') };
  render() {
    return (
      <WebView
        source={{ uri: this.state.URL}}
        style={{ flex: 1, backgroundColor: '#F95A37', marginTop: 20 }}
      />
    );
  }
}

export default DetailsScreen