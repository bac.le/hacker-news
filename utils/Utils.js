import moment from 'moment'

export const toRelativeTime = (unixTime) => {
    let dateString = moment.unix(unixTime).format("YYYYMMDDHHmm");
    return moment(dateString, "YYYYMMDDHHmm").fromNow()
}